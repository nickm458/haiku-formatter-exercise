public class Main {
    public static void main(String[] args) {
        String [][] strings = {
                { "An", "Old", "Silent", "Pond..." },
                { "A", "frog", "jumps", "into", "the", "pond," },
                { "splash!", "Silence", "again." }
        };

        System.out.println();

        for (String[] line : strings) {
            String stringToPrint = "";
            for (String word : line) {
                stringToPrint = stringToPrint.concat(word).concat(" ");
            }
            stringToPrint = stringToPrint.substring(0, stringToPrint.length()-2);
            System.out.println(stringToPrint);
        }
    }


//    Original attempt:
//    String array[] = { "An Old Silent Pond..." , "A frog jumps into the pond," , "Splash! Silence again." };
//
//        for (String x : array) {
//            System.out.println(x);
//        }
}
